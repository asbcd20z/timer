extern crate chrono;
use chrono::offset::Utc;
use chrono::DateTime;
use chrono::FixedOffset;
use std::fs;
use std::thread;
use std::time::SystemTime;

fn main() {
    let prev_secs: i8 = 0;
    loop {
        let system_time = SystemTime::now();
        let datetime: DateTime<Utc> = system_time.into();
        let secs_string = datetime.format("%S").to_string();
        let secs: i8 = secs_string.parse().unwrap_or_default();

        let mut output: String = datetime
            .with_timezone(&FixedOffset::east(2 * 3600))
            .format("%H:%M")
            .to_string();
        output = output + "\n";

        let mut i: i8 = 0;
        while i < (secs / 10) {
            output = output + "˙";
            i = i + 1;
        }
        while i < 5 {
            output = output + "˚";
            i = i + 1;
        }
        if prev_secs != secs {
            fs::write("timestamp.txt", output).unwrap();
            thread::sleep(std::time::Duration::from_secs(5));
        } else {
            thread::sleep(std::time::Duration::from_secs(1));
        }
        let prev_secs = secs;
    }
}
