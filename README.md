///gitlab-edit-push?
///more

# Timer
Simple program which updates the current time in a file.
I use this to show the current time on my webcam by using OBS.

## Output
This program creates the file ```timestamp.txt``` in your current working dir.

```
08:49
˙˙˚˚˚
```

The dots in the bottom row indicate the number of seconds in the current minute. Each closed dot represents 10 seconds have passed. In the example above the current exact time is between 08:49:20 and 08:49:30
